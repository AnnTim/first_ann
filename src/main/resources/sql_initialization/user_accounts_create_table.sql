create table app.user_accounts
(
id bigint not null ,
login varchar not null unique ,
password varchar not null

);

create sequence app.user_accounts_id_seq;
alter table app.user_accounts alter column id set default nextval('app.user_accounts');
alter sequence app.user_accounts_id_seq owned by app.user_accounts.id;

alter table app.user_accounts add constraint user_accounts_pk primary key (id);
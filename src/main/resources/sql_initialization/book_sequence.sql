create sequence app.books_id_seq;

alter table app.books alter column id set default nextval('app.books_id_seq');

alter sequence app.books_id_seq owned by app.books.id;

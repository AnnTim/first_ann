create table app.books
(
    id     bigint  not null
        constraint books_pkey
            primary key,
    author varchar not null,
    genre  varchar not null,
    name   varchar not null,
    year   date
);
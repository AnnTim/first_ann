create table app.book_user_connection
(
    user_id   bigint not null,
    book_id bigint not null
);

create unique index idx_un_book_user on app.book_user_connection(user_id, book_id);

alter table app.book_user_connection add constraint fk_buc_user_id_users foreign key (user_id) references app.users(id);

alter table app.book_user_connection add constraint fk_buc_book_id_books foreign key (book_id) references app.books(id);

alter table app.book_user_connection add primary key(user_id, book_id);
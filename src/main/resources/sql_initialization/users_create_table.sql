create table app.users
(
    id         bigint  not null
        constraint users_pkey
            primary key,
    nickname   varchar not null,
    count_book integer default 0
);

alter table app.users add column account_id bigint;

alter table app.users alter column account_id set not null ;

alter table app.users add constraint fk_users_account_id foreign key(account_id) references app.user_accounts(id);
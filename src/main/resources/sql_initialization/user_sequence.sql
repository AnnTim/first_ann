create sequence app.users_id_seq;

alter table app.users alter column id set default nextval('app.users_id_seq');

alter sequence app.users_id_seq owned by app.users.id;
package ann.repository;

import ann.domain.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface BookRepository extends JpaRepository<Book, Long> {
    @Query(nativeQuery = true, value = "select * from app.books b where lower(b.name) like lower(:name)")
    List<Book> findAllByNameContaining(@Param("name") String name);

    @Query(nativeQuery = true, value = "select * from app.books where lower(author) like lower(:author)")
    List<Book> findAllByAuthorContaining(@Param("author") String author);

    @Query(nativeQuery = true, value = "select * from app.books b where lower(b.author) like lower(:author) and lower(b.name) like lower(:name)")
    List<Book> findAllByNameContainingAndAuthorContaining(@Param("name") String name, @Param("author") String author);

    boolean existsById(long id);
}

package ann.repository;

import ann.domain.BookUserConnection;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookUserConnectionRepository extends JpaRepository<BookUserConnection, BookUserConnection.BookUserConnectionId> {
    boolean existsByBookIdAndUserId(long bookId, long userId);
}

package ann.domain;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@Getter
@Entity
@Table(schema = "app", name = "users")
public class User {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "app.users_id_seq")
    @SequenceGenerator(name = "app.users_id_seq", sequenceName = "app.users_id_seq", allocationSize = 1)
    private Long id;

    @Column(name = "nickname")
    private String nickname;

    @Column(name = "count_book")
    private int countBook; //Количество прочитанных книг

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(schema = "app", name = "book_user_connection", joinColumns = {@JoinColumn(name = "user_id")}, inverseJoinColumns = {@JoinColumn(name = "book_id")})
    private List<Book> bookList = new ArrayList<>();

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "account_id", referencedColumnName = "id")
    private Account account;


    public User(String nickname, Account account) {
        this.nickname = nickname;
        this.account = account;
    }

    public void plusCount(){
        this.countBook = this.countBook++;
    }
    public void minusCount(){
        this.countBook = this.countBook--;
    }


}

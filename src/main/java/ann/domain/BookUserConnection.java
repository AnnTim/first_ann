package ann.domain;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@IdClass(BookUserConnection.BookUserConnectionId.class)
@Table(schema = "app", name = "book_user_connection")
public class BookUserConnection {

    @Id
    @Column(name = "user_id")
    private Long userId;
    @Id
    @Column(name = "book_id")
    private Long bookId;

    @NoArgsConstructor
    @AllArgsConstructor
    public static class BookUserConnectionId implements Serializable {
        private Long userId;

        private Long bookId;
    }

}


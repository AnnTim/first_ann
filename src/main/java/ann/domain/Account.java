package ann.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@Entity
@Table(schema = "app", name = "user_accounts")
public class Account {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "app.user_accounts_id_seq")
    @SequenceGenerator(name = "app.user_accounts_id_seq", sequenceName = "app.user_accounts_id_seq", allocationSize = 1)
    private Long id;

    @Column(name = "login")
    private String login;

    @Column(name = "password")
    private String password;

    public Account(String login, String password){
        this.login = login;
        this.password = password;
    }
}

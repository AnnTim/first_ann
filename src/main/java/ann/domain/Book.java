package ann.domain;

import ann.enumerated.Genre;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Data
@Entity
@Table(schema = "app", name = "books")
public class Book {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "app.books_id_seq")
    @SequenceGenerator(name = "app.books_id_seq", sequenceName = "app.books_id_seq", allocationSize = 1)
    private Long id;

    @Column(name = "author")
    private String author;

    @Column(name = "genre")
    @Enumerated(EnumType.STRING)
    private Genre genre;

    @Column(name = "name")
    private String name;

    @Column(name = "year")
    @DateTimeFormat(pattern = "dd.MM.yyyy")
    private LocalDate year;

}



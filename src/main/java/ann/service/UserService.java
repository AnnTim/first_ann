package ann.service;

import ann.domain.User;
import ann.dto.UserDTO;
import ann.error.AnnException;
import ann.mapper.UserMapper;
import ann.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    private final UserMapper userMapper;

    public User getUserById(Long id) {
        Optional<User> user = userRepository.findById(id);
        return user.orElse(null);
    }

    public List<User> getAllByNickname(String nickname) {
        return userRepository.findAllByNicknameContaining("%" + nickname + "%");
    }

    public String save(UserDTO userDTO) throws AnnException {
        User user;
        String message;
        if (userDTO.getId() != null) {
            Optional<User> userOptional = userRepository.findById(userDTO.getId());
            if (!userOptional.isPresent()) {
                throw new AnnException("User is not found");
            }
            user = userOptional.get();
            user.setNickname(userDTO.getLogin());
            user.getAccount().setLogin(userDTO.getLogin());
            user.getAccount().setPassword(userDTO.getPassword());
            message = "updated";

        } else {
            user = userMapper.fromDTO(userDTO);
            message = "created";
        }

        try {
            userRepository.save(user);
            return "User is successfully " + message + "!";
        } catch (DataIntegrityViolationException e) {
            System.out.println(e.getMessage());
            throw new AnnException("This nickname is already taken");
        } catch (Exception e) {
            throw new AnnException("Something went wrong");
        }
    }

    public void plusCount(Long userId) {
        User user = getUserById(userId);
        user.plusCount();
        userRepository.save(user);
    }

    public void minusCount(Long userId){
        User user = getUserById(userId);
        user.minusCount();
        userRepository.save(user);
    }
}

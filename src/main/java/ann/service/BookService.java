package ann.service;

import ann.domain.Book;
import ann.error.AnnException;
import ann.repository.BookRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class BookService {
    private final BookRepository bookRepository;

    public Book getBookById(Long id) {
        return bookRepository.findById(id).orElse(null);
    }

    public List<Book> getAll(String name, String author) {
        if (name != null && author != null) {
            return bookRepository.findAllByNameContainingAndAuthorContaining("%" + name + "%", "%" + author + "%");
        }

        if (name != null) {
            return bookRepository.findAllByNameContaining("%" + name + "%");
        }

        if (author != null) {
            return bookRepository.findAllByAuthorContaining("%" + author + "%");
        }

        return bookRepository.findAll();
    }

    public void save(Book book) throws AnnException {
        if (book.getId() != null) {
            if (!bookRepository.existsById(book.getId())) {
                throw new AnnException("Book is not found");
            }
        }
        bookRepository.save(book);
    }

}

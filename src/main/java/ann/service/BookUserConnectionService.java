package ann.service;

import ann.domain.BookUserConnection;
import ann.error.AnnException;
import ann.repository.BookUserConnectionRepository;
import ann.validation.BookUserConnectionValidator;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class BookUserConnectionService {
    private final BookUserConnectionRepository bookUserConnectionRepository;
    private final BookUserConnectionValidator bookUserConnectionValidator;
    private final UserService userService;

    public void save(BookUserConnection bookUserConnection) throws AnnException {
        bookUserConnectionValidator.validate(bookUserConnection);
        if(bookUserConnectionRepository.existsByBookIdAndUserId(
                bookUserConnection.getBookId(), bookUserConnection.getUserId())
        ){
            throw new AnnException("This user already have this book");
        }
        bookUserConnectionRepository.save(bookUserConnection);
        userService.plusCount(bookUserConnection.getUserId());
    }

    public void delete(BookUserConnection bookUserConnection) throws AnnException {
        bookUserConnectionValidator.notNullValidate(bookUserConnection);
        if (!bookUserConnectionRepository.existsByBookIdAndUserId(
                bookUserConnection.getBookId(),bookUserConnection.getUserId())
        ){
            throw new AnnException("This user don't have this book");
        }
        bookUserConnectionRepository.delete(bookUserConnection);
        userService.minusCount(bookUserConnection.getUserId());
    }
}

package ann.mapper;

import ann.domain.Account;
import ann.domain.User;
import ann.dto.UserDTO;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {
    public User fromDTO(UserDTO dto){

        Account account = new Account(dto.getLogin(), dto.getPassword());

        return new User(dto.getLogin(), account);

    }
}

package ann.web;

import ann.domain.User;
import ann.dto.UserDTO;
import ann.error.AnnException;
import ann.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @GetMapping("/user/{id}")
    public User findUserById(@PathVariable Long id) {
        return userService.getUserById(id);
    }

    @GetMapping("/user")
    public List<User> findUser(@RequestParam String nickname) {
        return userService.getAllByNickname(nickname);
    }

    @PostMapping("/user")
    public ResponseEntity<String> saveUser(@RequestBody UserDTO userDTO) {
        try {
            return ResponseEntity.ok(userService.save(userDTO));
        } catch (AnnException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }
}

package ann.web;

import ann.domain.Book;
import ann.error.AnnException;
import ann.service.BookService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class BookController {

    private final BookService bookService;

    @GetMapping("/book/{id}")
    public Book findBookById(@PathVariable Long id) {
        return bookService.getBookById(id);
    }

    @GetMapping("/book")
    public List<Book> findBook(@RequestParam(required = false) String name, @RequestParam(required = false) String author) {
        return bookService.getAll(name, author);
    }

    @PostMapping("/book")
    public ResponseEntity<String> saveBook(@RequestBody Book book) {
        try {
            bookService.save(book);
            return ResponseEntity.ok("Success!!!");
        } catch (AnnException e) {
            System.out.println(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Not success: " + e.getMessage());
        }
    }

}

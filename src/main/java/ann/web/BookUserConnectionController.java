package ann.web;

import ann.domain.BookUserConnection;
import ann.error.AnnException;
import ann.service.BookUserConnectionService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequiredArgsConstructor
public class BookUserConnectionController {
    private final BookUserConnectionService service;

    @PostMapping("/book-user-connection")
    public ResponseEntity<String> addBookToUser(@RequestBody BookUserConnection connection) {
        try {
            service.save(connection);
            return ResponseEntity.ok("Book is successfully added to user");
        } catch (AnnException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @DeleteMapping("/book-user-connection")
    public ResponseEntity<String> deleteBookFromUser(@RequestBody BookUserConnection connection) {
        try {
            service.delete(connection);
            return ResponseEntity.ok("Book is successfully deleted from user");
        } catch (AnnException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }
}

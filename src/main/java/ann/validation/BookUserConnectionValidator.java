package ann.validation;

import ann.domain.BookUserConnection;
import ann.error.AnnException;
import ann.repository.BookRepository;
import ann.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class BookUserConnectionValidator {

    private final BookRepository bookRepository;
    private final UserRepository userRepository;

    public void validate(BookUserConnection bookUserConnection) throws AnnException {
        notNullValidate(bookUserConnection);
        if (!bookRepository.existsById(bookUserConnection.getBookId())){
            throw new AnnException("There is no book found by this id");
        }
        if (!userRepository.existsById(bookUserConnection.getUserId())){
            throw new AnnException("There is no user found by this id");
        }

    }

    public void notNullValidate(BookUserConnection bookUserConnection) throws AnnException {
        if (bookUserConnection.getBookId()==null || bookUserConnection.getUserId()==null){
            throw new AnnException("Book id or User id is null, you're idiot!");
        }
    }

}

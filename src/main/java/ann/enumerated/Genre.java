package ann.enumerated;

public enum Genre {
    NOVEL, POEM;
}

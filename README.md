Проект First_ann - это api для составления персональных списков книг. 

Описание методов:

1. User:
    1. Создание/обновление пользователя. Запрос POST с URI "/user" и телом {"id":number, "login":"string", "password":"string"}.
    2. Поиск пользователя по id. Запрос GET с URI "/user/{id}".
    3. Поиск пользователей. Запрос GET с URI "/user". Возможный параметр - nickname.
    
2. Book:
    1. Создание/обновление книги. Запрос POST с URI "/book" и телом {"id":number, "author":"string", "genre":"string", "name":"string", "year":"date"}. Поле "genre" ограниченно определенными значениями ("NOVEL", "POEM").
    2. Поиск книги по id. Запрос GET с URI "/book/{id}".
    3. Поиск книг. Запрос GET с URI "/book". Возможные параметры - name, author.
    
3. BookUserConnection:
    1. Добавление книги в список поользователя. Запрос POST с URI "/book-user-connection" и телом {"userId":number, "bookId":number}.
    2. Удаление книги из списка пользователя. Запрос DELETE с URI "/book-user-connection" и телом {"userId":number, "bookId":number}.
    
Что необходимо для запуска: 

1. Java 8
2. Postgres
3. Создать БД и запустить SQL-скрипты из папки src/main/resources/sql_initialization в следующей последовательности: 
    1. create_schema.sql;
    2. user_accounts_create_table.sql;
    3. users_create_table.sql;
    4. user_sequence.sql;
    5. books_create_table.sql
    6. book_sequence.sql;
    7. book_user_connection_create_table.sql.
4. Прописать настройки БД в файл src/main/resources/config/application.properties.

Запуск проекта:

1. Выполнить сборку из корня проекта - mvnw clean package.
2. Запустить cmd/terminal и выполнить команду java -jar {full path}/app.jar
     
